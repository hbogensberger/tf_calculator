__Term Frequency Calculator__

Takes as input a set of documents and a list of words, and returns the document with the highest term frequency score for each word and the term frequency score for that word in that document. To break the document into words, punctuation was stripped out, split by whitespace, and converted to lowercase.

__Getting Started__

These instructions will get you a copy of the project up and running on your local machine.

You will need to clone the Python Module. Use the following instructions:
1)Navigate to the repository in Bitbucket.
2)Click the Clone button.
3)Copy the clone command (either the SSH format or the HTTPS).
4)If you are using the SSH protocol, ensure your public key is in Bitbucket and loaded on the local system to which you are cloning.
5)Launch a terminal window.
6)Change to the local directory where you want to clone your repository.
Paste the command you copied from Bitbucket, for example:
Clone over HTTPS:
$ https://username@bitbucket.org/teamsinspace/documentation-tests.git
Clone over SSH:
$ git@bitbucket.org:teamsinspace/documentation-tests.git

If the clone was successful, a new sub-directory appears on your local drive. This directory has the same name as the Bitbucket repository that you cloned.

You can now run the program

__Example Result__
Given the documents in the data folder and the following words: “queequeg”, “whale”, and “sea”. The resulting output is:

`{'whale': [0.0013544018058690745, 'data/mobydick-chapter1.txt'], 'queequeg': [0.0066344993968636915, 'data/mobydick-chapter4.txt'], 'sea': [0.004514672686230248, 'data/mobydick-chapter1.txt']}`

__Authors__
Hannah Bogensberger