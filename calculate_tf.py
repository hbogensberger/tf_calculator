"""
Takes as input a set of documents and words. Returns the document with the highest term frequency score and the term frequency score.
"""

import string

def get_concordance(document):
    """Returns a dictionary of all words in document and their number of occurrences and total number of words."""
    frequency = {}

    with open(document, 'r') as myfile:
        text = myfile.read().replace('\n', ' ').lower()
        words = [word.strip(string.punctuation) for word in text.split()]
    myfile.close

    for word in words:
        if word not in frequency:
            frequency[word] = 1
        else:
            frequency[word] += 1
    return frequency, len(words)


def calculate_tf(document_concordance, word):
    """Returns TF score of word."""
    frequency, length = document_concordance
    if word in frequency:
        score = float(frequency[word])/length
        return score
    return 0.0


def get_tfscore(documents, words):
    """Returns TF score for each word and corresponding document
    that produced the highest that score."""
    tracker = {}
    for document in documents:
        doc_frequency = get_concordance(document)
        for word in words:
            tf = calculate_tf(doc_frequency, word)
            if word not in tracker:
                tracker[word] = [tf, document]
            else:
                if tracker[word][0] < tf:
                    tracker[word] = [tf, document]
    return tracker


def main():
    chap1 =  'data/mobydick-chapter1.txt'
    chap2 = 'data/mobydick-chapter2.txt'
    chap3 = 'data/mobydick-chapter3.txt'
    chap4 = 'data/mobydick-chapter4.txt'
    chap5 = 'data/mobydick-chapter5.txt'

    documents = [chap1, chap2, chap3, chap4, chap5]
    words = ['whale', 'queequeg', 'sea']
    output = get_tfscore(documents, words)

    print output

if __name__ == "__main__":
    main()
